# V3 API Automation

-------------------
# What is it?

This project comprises a scalable, dynamic, configurable and portable test harness, the purpose of which is the automation of API test cases related to the Emergent Payments, Inc. V3 API.

* Uses the [Cucumber BDD](https://cucumber.io/) collaboration tool
* Glue code used to execute Cucumber step-definitions is written in [Java](https://en.wikipedia.org/wiki/Java_(programming_language))
* The [Selenium WebDriver](http://www.seleniumhq.org/projects/webdriver/) and [Rest-Assured](https://github.com/jayway/rest-assured) libraries are used to interact with the API calls, respectively

# Getting Started

* This is a Maven project. The included pom.xml includes all dependencies required for the project to execute correctly
* From the bottom up:
    * Test cases are contained in the Cucumber \*.feature files
    * Each feature file may contain several scenarios or scenario outlines
    * Cucumber uses a set of @ tags to give greater control when executing the feature.
    * This project contains tags to specify the type of feature, and API sepcific tags (like @INTERNAL or @PUBLIC) to specify to the JUnit class which features to run
    * A series of JUnit clases have been established to drive suite-based execution, based on the tagging process mentioned above
    * To account for future CI needs, the Maven Surefire and Maven-Cucumber-Reporting plugins have been added to the project
    * The plugins described in the line above make it possible to run the full set, or a subset of the JUnit suites and report on the test results in a transparent way
    * This project is pre-configured to be run from Jenkins, from any git repository. System Variables are included in initializing the Hooks class to account for parametrized building in Jenkins as well
* The *.properties file also contains url, username and password specifics. These are initialized in the Hooks class and are used as variables throughout the project. Please enusre that they are also relevant to your configuration for the SUT

* How to run tests:
    * Tests can be run in multiple ways: a single feature file, a single JUnit suite, or as a group of various configured suites via a maven build (cmd maven clean install)
    * Simply select "run as" on any feature file, or JUnit suite. OR, run the command line and use "mvn clean install" to execute all test suites

# Who do I talk to?

* This repository is maintained by Ryan Bedino:
    * Email: saieph@icloud.com
    * Skype: gashzilla