#@Ignore
@testscenario
Feature: V3 API - Tester


  Scenario Outline: Public GET Healthcheck Test
    Given The "test case" for the scenario is '<testCase>'
    Given The "test purpose" for the scenario is '<testPurpose>'
    When I try to check the GET Healthcheck responses with '<validation>' credentials '<merchantAccount>', '<callerName>', '<secretKey>', '<onBehalfOf>'
    Then I should see that the response types are correct for "api/v3/healthcheck"

    Examples:
      |validation|testPurpose|testCase|merchantAccount|callerName   |secretKey |onBehalfOf  |
      |valid	 |*          |69445	  |BLUEFISH       |$apiTestSuper|Password12|*           |
      |valid	 |*          |69446	  |BLUEFISH       |$apiTestSuper|Password12|SQAUTOMATION|
      |valid	 |*          |69447	  |SQAUTOMATION   |$apiTest     |Password12|*           |
      |access	 |*          |69448	  |SQAUTOMATION   |$apiTestSuper|Password12|*           |



  Scenario Outline: Public POST /customers Endpoint
    Given The "test case" for the scenario is '<testCase>'
    Given The "test purpose" for the scenario is '<testPurpose>'
    When I try to check the POST /customers responses with '<validation>' credentials '<merchantAccount>', '<callerName>', '<secretKey>', '<onBehalfOf>'
    Then I should see that the response types are correct for "api/v3/customers POST"

    Examples:
      |validation|testPurpose|testCase|merchantAccount|callerName|secretKey |onBehalfOf|
      |valid	 |*          |69532   |SQAUTOMATION   |$apiTest  |Password12|*         |
      |invalid	 |*          |69533   |SQAUTOMATION   |$apiTest  |Password12|*         |
      |param	 |*          |69534   |SQAUTOMATION   |$apiTest  |Password12|*         |
      |access	 |*          |69535   |BLUEFISH       |$apiTest  |Password12|*         |



  Scenario Outline: Public POST /paymentprofiles Endpoint
    Given The "test case" for the scenario is '<testCase>'
    Given The "test purpose" for the scenario is '<testPurpose>'
    When I try to check the POST /charges responses with '<special>' '<profileType>' credentials '<merchantAccount>', '<callerName>', '<secretKey>', '<onBehalfOf>'
    When I try to check the POST /paymentprofiles responses with '<validation>' '<profileType>' credentials '<merchantAccount>', '<callerName>', '<secretKey>', '<onBehalfOf>'
    Then I should see that the response types are correct for "api/v3/paymentprofiles POST"

    Examples:
      |validation|profileType|testPurpose|testCase|special|merchantAccount|callerName|secretKey |onBehalfOf|
      |valid	 |credit card|*          |69501   |oneOff |SQAUTOMATION   |$apiTest  |Password12|*         |
      |invalid	 |credit card|*          |69502   |oneOff |SQAUTOMATION   |$apiTest  |Password12|*         |
      |param	 |credit card|*          |69503   |oneOff |SQAUTOMATION   |$apiTest  |Password12|*         |
      |access	 |credit card|*          |69504   |oneOff |BLUEFISH       |$apiTest  |Password12|*         |



  Scenario Outline: Public DELETE /paymentprofiles/id Endpoint
    Given The "test case" for the scenario is '<testCase>'
    Given The "test purpose" for the scenario is '<testPurpose>'
    When I try to check the POST /charges responses with '<special>' '<profileType>' credentials '<merchantAccount>', '<callerName>', '<secretKey>', '<onBehalfOf>'
    When I try to check the POST /paymentprofiles responses with '<special>' '<profileType>' credentials '<merchantAccount>', '<callerName>', '<secretKey>', '<onBehalfOf>'
    When I try to check the DELETE /paymentprofiles/id responses with '<validation>' '<profileType>' credentials '<merchantAccount>', '<callerName>', '<secretKey>', '<onBehalfOf>', '<sppId>'
    Then I should see that the response types are correct for "api/v3/paymentprofiles/id DELETE"

    Examples:
      |validation|profileType|testPurpose|testCase|special|merchantAccount|callerName|secretKey |onBehalfOf|sppId|
      |valid	 |credit card|*          |69509   |oneOff |SQAUTOMATION   |$apiTest  |Password12|*         |*    |
      |invalid	 |credit card|*          |69510   |oneOff |SQAUTOMATION   |$apiTest  |Password12|*         |k    |
      |param	 |credit card|*          |69511   |oneOff |SQAUTOMATION   |$apiTest  |Password12|*         |6666 |
      |access	 |credit card|*          |69512   |oneOff |BLUEFISH       |$apiTest  |Password12|*         |*    |