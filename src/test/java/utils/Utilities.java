package utils;

import org.apache.commons.io.IOUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import step_definitions.Hooks;
import step_definitions.SharedStepDefs;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;


/**
 * Created by rbedino on 1/11/2016
 */


public class Utilities {
    private static final String HMAC_SHA256_ALGORITHM = "HmacSHA256";
    public static String requestPath;
    public static String requestBody;

    public static String timestamp;
    static String jsonString0;
    static String jsonString1;
    public static String jsonString;
    static String modifiedString;
    static String validFieldsJson;
    static String newRequestBody;

    public static String getTimestamp(){
        timestamp = String.valueOf(System.currentTimeMillis()/1000);
        return timestamp;
    }

    public static String getJsonString(String inputJson) throws IOException {
        InputStream inputstream = new FileInputStream(inputJson);
        String myString = IOUtils.toString(inputstream, "UTF-8");
        try {
            JSONObject json = (JSONObject) new JSONParser().parse(myString);
            jsonString = json.toString();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        inputstream.close();
        return jsonString;
    }

    public static String updateJsonString(String inputJson, String[] fieldArray) throws IOException {


        int index = 1;

//        position in the array. field is the first element and value is the second element
        int fieldIndex = 0;
        int valueIndex = 1;

        while ( valueIndex  < fieldArray.length ){

            System.out.println(index);
            System.out.println("FIELD "+ index +" = "+fieldArray[fieldIndex]);
            System.out.println("VALUE "+ index +" = "+fieldArray[valueIndex]);
//            Prints Corresponding Value
            index+=1;
            fieldIndex+=2;
            valueIndex+=2;
        }

        InputStream inputstream = new FileInputStream(inputJson);
        String myString = IOUtils.toString(inputstream, "UTF-8");
        try {

            JSONObject json = (JSONObject) new JSONParser().parse(myString);
            modifiedString = json.toString();

        } catch (ParseException e) {
            e.printStackTrace();
        }


        for (valueIndex = 1; valueIndex  < fieldArray.length; valueIndex +=2) {

            jsonString = modifiedString.replace(fieldArray[valueIndex - 1], fieldArray[valueIndex]);
            modifiedString = jsonString;
        }

        inputstream.close();

        System.out.println("This is the json body = "+jsonString);
        return jsonString;
    }


    public static String missingFieldJsonString(String inputJson, String outerNestField,String outerNestFieldCC, String innerNestField, String [] fieldArray) throws IOException{
        int index = 1;

//        position in the array. field is the first element and value is the second element
        int fieldIndex = 0;
        int valueIndex = 1;

        while ( valueIndex  < fieldArray.length ){

            System.out.println(index);
            System.out.println("FIELD "+ index +" = "+fieldArray[fieldIndex]);
            System.out.println("VALUE "+ index +" = "+fieldArray[valueIndex]);
//            Prints Corresponding Value
            index+=1;
            fieldIndex+=2;
            valueIndex+=2;
        }

        InputStream inputstream = new FileInputStream(inputJson);
        String myString = IOUtils.toString(inputstream, "UTF-8");
        try {

            JSONObject json = (JSONObject) new JSONParser().parse(myString);
            modifiedString = json.toString();


        } catch (ParseException e) {
            e.printStackTrace();
        }

        for (valueIndex = 1; valueIndex  < fieldArray.length; valueIndex +=2) {

            jsonString = modifiedString.replace(fieldArray[valueIndex - 1], fieldArray[valueIndex]);
            modifiedString = jsonString;
        }

        inputstream.close();

        System.out.println("\n This is the json request body = "+jsonString);

//   Delete the required nested field
        if(outerNestFieldCC.equals("creditCardPayment") || outerNestFieldCC.equals("callbackPayment") || outerNestFieldCC.equals("recorderPayment") || outerNestFieldCC.equals("check")) {
            try {
                JSONObject missingFieldsJson = (JSONObject) new JSONParser().parse(jsonString);
                JSONArray nestedArray = (JSONArray) missingFieldsJson.get(outerNestField);
                JSONObject nestedArray1 = (JSONObject) nestedArray.get(0);
                JSONObject nestedArray2 = (JSONObject) nestedArray1.get(outerNestFieldCC);
                nestedArray2.remove(innerNestField);
                jsonString = missingFieldsJson.toString();
                System.out.println("]\n Json request body with" + " " + outerNestField+ "." +innerNestField + " " +"missing:" + missingFieldsJson.toString() + "\n");

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        else{
            try {
                JSONObject missingFieldsJson = (JSONObject) new JSONParser().parse(jsonString);
                JSONObject nestedArray = (JSONObject) missingFieldsJson.get(outerNestField);
                nestedArray.remove(innerNestField);
                jsonString = missingFieldsJson.toString();
                System.out.println("\n Json request body with" + " " + outerNestField+ "." +innerNestField + " " +"missing:" + missingFieldsJson.toString()+"\n");

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return jsonString;
    }

    //Method to delete the value of the nested field
    public static String missingValueJsonString(String inputJson, String outerNestField,String outerNestFieldCC, String innerNestField, String [] fieldArray) throws IOException{

        int index = 1;

//        position in the array. field is the first element and value is the second element
        int fieldIndex = 0;
        int valueIndex = 1;

        while ( valueIndex  < fieldArray.length ){

            System.out.println(index);
            System.out.println("FIELD "+ index +" = "+fieldArray[fieldIndex]);
            System.out.println("VALUE "+ index +" = "+fieldArray[valueIndex]);
//            Prints Corresponding Value
            index+=1;
            fieldIndex+=2;
            valueIndex+=2;
        }

        InputStream inputstream = new FileInputStream(inputJson);
        String myString = IOUtils.toString(inputstream, "UTF-8");
        try {

            JSONObject json = (JSONObject) new JSONParser().parse(myString);
            modifiedString = json.toString();


        } catch (ParseException e) {
            e.printStackTrace();
        }

        for (valueIndex = 1; valueIndex  < fieldArray.length; valueIndex +=2) {
            // Replaces respective template field name from json string with the required value from feature file. E.g. replaces customerCountry with values such as 'US' or 'FR' from feature file
            jsonString = modifiedString.replace(fieldArray[valueIndex - 1], fieldArray[valueIndex]);

            modifiedString = jsonString;
        }

        inputstream.close();

        System.out.println("\n This is the json request body = "+jsonString);

//   Delete the required nested field
        if(outerNestFieldCC.equals("creditCardPayment") || outerNestFieldCC.equals("callbackPayment") || outerNestFieldCC.equals("recorderPayment") || outerNestFieldCC.equals("check")) {
            try {
                JSONObject missingFieldsJson = (JSONObject) new JSONParser().parse(jsonString);
                JSONArray nestedArray = (JSONArray) missingFieldsJson.get(outerNestField);
                JSONObject nestedArray1 = (JSONObject) nestedArray.get(0);
                JSONObject nestedArray2 = (JSONObject) nestedArray1.get(outerNestFieldCC);
                nestedArray2.put(innerNestField, "");
                jsonString = missingFieldsJson.toString();
                System.out.println("\n Json request body with" + " " + outerNestField+ "." +innerNestField + " " +"missing:" + jsonString + "\n");

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        else{
            try {
                JSONObject missingFieldsJson = (JSONObject) new JSONParser().parse(jsonString);
                JSONObject nestedArray = (JSONObject) missingFieldsJson.get(outerNestField);
                nestedArray.put(innerNestField, "");
                jsonString = missingFieldsJson.toString();
                System.out.println("\n Json request body with" + " " + outerNestField+ "." +innerNestField + " " +"missing:" + jsonString +"\n");

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return jsonString;
    }


    public static String missingDynamicFieldJsonString(String inputJson, String outerNestField,String outerNestFieldCC,String dynammicField,String field, int placeHolder , String missingField ,String name,String value, String [] fieldArray) throws IOException{
        System.out.println("FIELD 1 = "+fieldArray[0]);
        System.out.println("VALUE 1 = "+fieldArray[1]);
        System.out.println("FIELD 2 = "+fieldArray[2]);
        System.out.println("VALUE 2 = "+fieldArray[3]);
        System.out.println("FIELD 3 = "+fieldArray[4]);
        System.out.println("VALUE 3 = "+fieldArray[5]);

        InputStream inputstream = new FileInputStream(inputJson);
        String myString = IOUtils.toString(inputstream, "UTF-8");
        try {
            JSONObject json = (JSONObject) new JSONParser().parse(myString);
            modifiedString = json.toString();
            jsonString = modifiedString.replace(fieldArray[0], fieldArray[1]);
            if(fieldArray[2]!=null){
                jsonString0 = jsonString.replace(fieldArray[2], fieldArray[3]);
                jsonString = jsonString0;
            }
            if(fieldArray[4]!=null){
                jsonString1 = jsonString.replace(fieldArray[4], fieldArray[5]);
                jsonString = jsonString1;
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }

        inputstream.close();

        System.out.println("\n This is the json request body = "+jsonString);

//   Delete the required nested field
        if(outerNestFieldCC.equals("creditCardPayment") || outerNestFieldCC.equals("callbackPayment") || outerNestFieldCC.equals("recorderPayment") || outerNestFieldCC.equals("check")) {
            try {
                JSONObject missingFieldsJson = (JSONObject) new JSONParser().parse(jsonString);
                JSONArray nestedArray = (JSONArray) missingFieldsJson.get(outerNestField);
                JSONObject nestedArray1 = (JSONObject) nestedArray.get(0);
                JSONObject nestedArray2 = (JSONObject) nestedArray1.get(outerNestFieldCC);
                JSONObject nestedArray3= (JSONObject) nestedArray2.get(dynammicField);
                JSONArray nestedArray4= (JSONArray) nestedArray3.get(field);
                JSONObject nestedArray5 = (JSONObject) nestedArray4.get(placeHolder);
                nestedArray5.remove(name);
                nestedArray5.remove(value);
                jsonString = missingFieldsJson.toString();
                System.out.println("\n Json request body with" + " " + outerNestField+ "." +dynammicField + "." + missingField +" " +"missing:" + jsonString + "\n");

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        else{
            try {
                JSONObject missingFieldsJson = (JSONObject) new JSONParser().parse(jsonString);
                JSONObject nestedArray = (JSONObject) missingFieldsJson.get(outerNestField);
                nestedArray.put(name, "");
                jsonString = missingFieldsJson.toString();
                System.out.println("\n Json request body with" + " " + outerNestField+ "." +dynammicField + "." + missingField + " " +"missing:" + jsonString +"\n");

            } catch (ParseException e) {
                e.printStackTrace();
            };
        }

        return jsonString;
    }

    public static String deleteJsonString(String inputJson, String propertyToDelete, String[] fieldArray) throws IOException {
        InputStream inputstream = new FileInputStream(inputJson);
        String myString = IOUtils.toString(inputstream, "UTF-8");


        jsonString0 = myString.replace(fieldArray[0], fieldArray[1]);
        if(fieldArray[2]!=null){
            jsonString = jsonString0.replace(fieldArray[2], fieldArray[3]);

        } else
            jsonString = jsonString0;

//        System.out.println(jsonString);

        try {
            JSONObject json = (JSONObject) new JSONParser().parse(jsonString);
            json.remove(propertyToDelete);
            jsonString = json.toString();
            System.out.println(jsonString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        inputstream.close();

        return jsonString;
    }


    public static String getRequestBody(String apiName, String validationType, String[] requiredFields) throws IOException {
        String randomCust = SharedStepDefs.getRandomCust();
        String randomTxnId = SharedStepDefs.getRandomTxnId();
        String randomEmail = SharedStepDefs.getRandomEmail();

        switch(apiName){
            case "tax":
                switch(validationType) {
                    case "valid":
                        getJsonString(Hooks.validTaxData);
                        break;
                    case "invalid":
                        getJsonString(Hooks.invalidTaxData);
                        break;
                    case "param":
                        getJsonString(Hooks.paramTaxData);
                        break;
                    case "access":
                        getJsonString(Hooks.validTaxData);
                        break;
                    default:
                        break;
                }
                break;
            case "post /charges":

                String[] fieldArrayCharges = new String[]{"CustomerTestID", randomCust, "TestTxnId", randomTxnId, null, null};
                switch(validationType) {
                    case "valid":
                        switch(requiredFields[0]) {
                            case "credit card":
                                System.out.println("The CUSTOMER ID FOR THIS TRANS IS - "+fieldArrayCharges[1]);
                                System.out.println("The Txn ID FOR THIS TRANS IS - "+fieldArrayCharges[3]);
                                updateJsonString(Hooks.validChargesData, fieldArrayCharges);
                                break;
                            case "paypal":
                                System.out.println("The CUSTOMER ID FOR THIS TRANS IS - "+fieldArrayCharges[1]);
                                System.out.println("The Txn ID FOR THIS TRANS IS - "+fieldArrayCharges[3]);
                                updateJsonString(Hooks.validChargesData_PAYPAL, fieldArrayCharges);
                                break;
                            case "prepaid card":
                                System.out.println("The CUSTOMER ID FOR THIS TRANS IS - "+fieldArrayCharges[1]);
                                System.out.println("The Txn ID FOR THIS TRANS IS - "+fieldArrayCharges[3]);
                                updateJsonString(Hooks.postCharges_PREPAID, fieldArrayCharges);
                                break;
                            case "check":
                                System.out.println("The CUSTOMER ID FOR THIS TRANS IS - "+fieldArrayCharges[1]);
                                System.out.println("The Txn ID FOR THIS TRANS IS - "+fieldArrayCharges[3]);
                                updateJsonString(Hooks.postCharges_CHECK, fieldArrayCharges);
                                break;
                            case "recorder":
                                System.out.println("The CUSTOMER ID FOR THIS TRANS IS - "+fieldArrayCharges[1]);
                                System.out.println("The Txn ID FOR THIS TRANS IS - "+fieldArrayCharges[3]);
                                updateJsonString(Hooks.postCharges_RECORDER, fieldArrayCharges);
                                break;
                            case "SPP":
                                String[] fieldArraySPP = new String[]{"TestTxnId", randomTxnId, "CustomerTestID", requiredFields[2], "TestSPPId", requiredFields[1]};
                                System.out.println("The CUSTOMER ID FOR THIS TRANS IS - "+fieldArraySPP[3]);
                                System.out.println("The Txn ID FOR THIS TRANS IS - "+fieldArraySPP[1]);
                                System.out.println("The SPP ID FOR THIS TRANS IS - "+fieldArraySPP[5]);
                                updateJsonString(Hooks.postCharges_SPP, fieldArraySPP);
                                break;
                            default:
                                break;
                        }
                        break;
                    case "param":
                        switch(requiredFields[0]) {
                            case "credit card":
                                updateJsonString(Hooks.paramChargesData, fieldArrayCharges);
                                break;
                            case "paypal":
                                updateJsonString(Hooks.validChargesData_PAYPAL, fieldArrayCharges);
                                break;
                            case "credit card - missing command":
                                String fieldToDelete =  "command";
                                System.out.println("The CUSTOMER ID FOR THIS TRANS IS - "+fieldArrayCharges[1]);
                                System.out.println("The Txn ID FOR THIS TRANS IS - "+fieldArrayCharges[3]);
                                deleteJsonString(Hooks.paramRequiredField_CreditCard, fieldToDelete, fieldArrayCharges);
                                break;
                            default:
                                break;
                        }
                        break;
                    case "access":
                        switch(requiredFields[0]) {
                            case "credit card":
                                updateJsonString(Hooks.validChargesData, fieldArrayCharges);
                                break;
                            case "paypal":
                                updateJsonString(Hooks.validChargesData_PAYPAL, fieldArrayCharges);
                                break;
                            default:
                                break;
                        }
                        break;
                    default:
                        break;
                }
                break;
            case "post /charges/id":
                switch(validationType) {
                    case "valid":
                        getJsonString(Hooks.validUpdateChargesData);
                        break;
                    case "param":
                        getJsonString(Hooks.paramUpdateChargesData);
                        break;
                    case "access":
                        getJsonString(Hooks.validUpdateChargesData);
                        break;
                    default:
                        break;
                }
                break;
            case "charges/verification":
                String[] fieldArrayVerification = new String[]{"CustomerTestID", requiredFields[0], null, null, null, null};
                switch(validationType) {
                    case "valid":
                        updateJsonString(Hooks.validVerifyChargesData, fieldArrayVerification);
                        break;
                    case "param":
                        updateJsonString(Hooks.paramVerifyChargesData, fieldArrayVerification);
                        break;
                    case "access":
                        updateJsonString(Hooks.validVerifyChargesData, fieldArrayVerification);
                        break;
                    default:
                        break;
                }
                break;
            case "post paymentprofiles":
                String[] fieldArrayPymtProfileAdd = new String[]{"CustomerTestID", requiredFields[1], null, null, null, null};
                switch(validationType) {
                    case "valid":
                        switch(requiredFields[0]) {
                            case "credit card":
                                updateJsonString(Hooks.validCCPaymentProfile, fieldArrayPymtProfileAdd);
                                break;
                            default:
                                break;
                        }
                        break;
                    case "update":
                        switch(requiredFields[0]) {
                            case "credit card":
                                updateJsonString(Hooks.updateCCPaymentProfile, fieldArrayPymtProfileAdd);
                                break;
                            default:
                                break;
                        }
                        break;
                    case "invalid":
                        switch(requiredFields[0]) {
                            case "credit card":
                                updateJsonString(Hooks.invalidCCPaymentProfile, fieldArrayPymtProfileAdd);
                                break;
                            default:
                                break;
                        }
                        break;
                    case "param":
                        switch(requiredFields[0]) {
                            case "credit card":
                                updateJsonString(Hooks.paramCCPaymentProfile, fieldArrayPymtProfileAdd);
                                break;
                            default:
                                break;
                        }
                        break;
                    case "access":
                        switch(requiredFields[0]) {
                            case "credit card":
                                updateJsonString(Hooks.validCCPaymentProfile, fieldArrayPymtProfileAdd);
                                break;
                            default:
                                break;
                        }
                        break;
                    default:
                        break;
                }
                break;
            case "put systemalert":
                getJsonString(Hooks.systemAlertData);
                break;
            case "post paymentprofiles/id":
                switch(validationType){
                    case "valid":
                        getJsonString(Hooks.validCCPaymentProfilePOST);
                        break;
                    case "param":
                        break;
                    case "access":
                        break;
                }
                break;
            case "post customers":
                String[] fieldArrayCustomers = new String[]{"CustomerTestID", randomCust, "CustomerTestEMAIL", randomEmail, null, null};
                switch(validationType) {
                    case "valid":
                        updateJsonString(Hooks.validCustomer, fieldArrayCustomers);
                        break;
                    case "invalid":
                        getJsonString(Hooks.invalidCustomer);
                        break;
                    case "param":
                        getJsonString(Hooks.paramCustomer);
                        break;
                    case "access":
                        updateJsonString(Hooks.validCustomer, fieldArrayCustomers);
                        break;
                    default:
                        break;
                }
                break;
            case "post customers/id":
                getJsonString(Hooks.validCustomerId);
                break;
            case "put callers/":
                getJsonString(Hooks.callerNameData);
                break;
            case "put callers/callerName/passwords/tag":
                getJsonString(Hooks.updateCallerNamePassword);
                break;
            case "post callers":
                String[] fieldArrayCallerName = new String[]{"TestCallerName", randomCust, null, null, null, null};
                String[] fieldArrayCallerNameInvalid = new String[]{"TestCallerName", "", null, null, null, null};
                switch(validationType) {
                    case "valid":
                        updateJsonString(Hooks.postCallerData, fieldArrayCallerName);
                        break;
                    case "invalid":
                        updateJsonString(Hooks.postCallerData, fieldArrayCallerNameInvalid);
                        break;
                    case "access":
                        updateJsonString(Hooks.postCallerData, fieldArrayCallerName);
                        break;
                    default:
                        break;
                }
                break;
            case "post callers/password":
                String[] fieldArrayCallerPassInvalid = new String[]{"Secret12", "", null, null, null, null};
                switch(validationType) {
                    case "valid":
                        getJsonString(Hooks.updateCallerNamePassword);
                        break;
                    case "invalid":
                        updateJsonString(Hooks.updateCallerNamePassword, fieldArrayCallerPassInvalid);
                        break;
                    case "param":
                        getJsonString(Hooks.updateCallerNamePassword);
                        break;
                    case "access":
                        getJsonString(Hooks.updateCallerNamePassword);
                        break;
                    default:
                        break;
                }
                break;
            case "post test/notifications":
                getJsonString(Hooks.postNotification);
                break;
            case "post accountprovisioning":
                String[] fieldArrayProvision = new String[]{"TestMerchantName", randomCust, null, null, null, null};
                switch(validationType) {
                    case "valid":
                        updateJsonString(Hooks.postAccountProvisioning, fieldArrayProvision);
                        break;
                    case "param":
                        getJsonString(Hooks.paramPostAccountProvisioning);
                        break;
                    case "access":
                        updateJsonString(Hooks.postAccountProvisioning, fieldArrayProvision);
                        break;
                    default:
                        break;
                }
                break;
            case "post configuration":
                switch(validationType) {
                    case "valid":
                        getJsonString(Hooks.postConfiguration);
                        break;
                    case "param":
                        getJsonString(Hooks.paramPostConfiguration);
                        break;
                    case "access":
                        getJsonString(Hooks.postConfiguration);
                        break;
                    default:
                        break;
                }
                break;
            case "post configuration/secrets/secretKeyName":
                switch(validationType) {
                    case "valid":
                        getJsonString(Hooks.postConfigurationSecretKey);
                        break;
                    case "invalid":
                        getJsonString(Hooks.invalidPostConfigurationSecretKey);
                    case "param":
                        getJsonString(Hooks.postConfigurationSecretKey);
                        break;
                    case "access":
                        getJsonString(Hooks.postConfigurationSecretKey);
                        break;
                    default:
                        break;
                }
                break;
            case "verify missing required fields post /charges":
                fieldArrayCharges = new String[]{"CustomerTestID", randomCust, "TestTxnId", randomTxnId, null, null};
                String requiredFieldtoDelete;
                switch (validationType) {
                    case "param":
                        switch (requiredFields[0]){
                            case "command":
                                requiredFieldtoDelete = "command";
                                System.out.println("The CUSTOMER ID FOR THIS TRANS IS - "+fieldArrayCharges[1]);
                                System.out.println("The Txn ID FOR THIS TRANS IS - "+fieldArrayCharges[3]);
                                deleteJsonString(Hooks.missingFieldCreditCard, requiredFieldtoDelete, fieldArrayCharges);
                                break;
                            case "externalTxnId":
                                requiredFieldtoDelete = "externalTxnId";
                                System.out.println("The CUSTOMER ID FOR THIS TRANS IS - "+fieldArrayCharges[1]);
                                System.out.println("The Txn ID FOR THIS TRANS IS - "+fieldArrayCharges[3]);
                                deleteJsonString(Hooks.missingFieldCreditCard, requiredFieldtoDelete, fieldArrayCharges);
                                break;
                            case "customer":
                                requiredFieldtoDelete = "customer";
                                System.out.println("The CUSTOMER ID FOR THIS TRANS IS - "+fieldArrayCharges[1]);
                                System.out.println("The Txn ID FOR THIS TRANS IS - "+fieldArrayCharges[3]);
                                deleteJsonString(Hooks.missingFieldCreditCard, requiredFieldtoDelete, fieldArrayCharges);
                                break;
//                            case "id":
//                                requiredFieldtoDelete = "id";
//                                deleteJsonString(Hooks.missingFieldCreditCard, requiredFieldtoDelete,fieldArrayCharges);
//                                break;
                            default:
                                break;
                        }
                        break;
                    default:
                        break;

                }
                break;
            default:
                break;

        }

        if(jsonString !=null){
            requestBody = jsonString;
        } else requestBody = "";

        return requestBody;
    }



    public static String getRequestPath(String[] pathVariableArray) throws UnsupportedEncodingException {
        String basicPath = "/api/v3/";
        switch(pathVariableArray[0]){
            case "healthcheck":
                requestPath = basicPath+pathVariableArray[0];
                break;
            case "notificationqueue":
                requestPath = basicPath+"healthcheck/"+pathVariableArray[0];
                break;
            case "configuration":
                requestPath = basicPath+pathVariableArray[0];
                break;
            case "exchangerates":
                requestPath = basicPath+pathVariableArray[0]
                        +"?fromCurrency="+pathVariableArray[1]+"&toCurrency="+pathVariableArray[2];
                break;
            case "paymentmethods":
                requestPath = basicPath+pathVariableArray[0]
                        +"?method="+pathVariableArray[1]+"&methodType="+pathVariableArray[2]+"&country="+pathVariableArray[3]+"&currency="+pathVariableArray[4]
                        +"&price="+pathVariableArray[5]+"&customerId="+pathVariableArray[6]+"&device="+pathVariableArray[7]+"&interface="+pathVariableArray[8]
                        +"&supportsPaymentProfile="+pathVariableArray[9];
                break;
            case "countries":
                requestPath = basicPath+pathVariableArray[0];
                break;
            case "encryptionkey":
                requestPath = basicPath+"configuration/"+pathVariableArray[0]
                        +"?forceNew="+pathVariableArray[1]+"&format="+pathVariableArray[2];
                break;
            case "charges":
                requestPath = basicPath+pathVariableArray[0]
                        +"?page="+pathVariableArray[1]+"&size="+pathVariableArray[2]+"&includeCount="+pathVariableArray[3]+"&id="+pathVariableArray[4]
                        +"&customerId="+pathVariableArray[5]+"&orderStates="+pathVariableArray[6]+"&createdAfter="+pathVariableArray[7]
                        +"&createdBefore="+pathVariableArray[8]+"&modifiedAfter="+pathVariableArray[9]+"&modifiedBefore="+pathVariableArray[10]
                        +"&paymentMethod="+pathVariableArray[11]+"&paymentType="+pathVariableArray[12]+"&currency="+pathVariableArray[13]
                        +"&gatewayProvidedId="+pathVariableArray[14]+"&properties="+pathVariableArray[15]+"&sort="+pathVariableArray[16];
                break;
            case "charges/":
                requestPath = basicPath+pathVariableArray[0]
                        +pathVariableArray[1];
                break;
            case "paymentprofiles":
                requestPath = basicPath+pathVariableArray[0]
                        +"?page="+pathVariableArray[1]+"&size="+pathVariableArray[2]+"&includeCount="+pathVariableArray[3]+"&name="+pathVariableArray[4]
                        +"&customerId="+pathVariableArray[5]+"&expirationDate="+pathVariableArray[6]+"&memo="+pathVariableArray[7]+"&methodType="+pathVariableArray[8];
                break;
            case "paymentprofiles/":
                requestPath = basicPath+pathVariableArray[0]
                        +pathVariableArray[1];
                break;
            case "customers":
                requestPath = basicPath+pathVariableArray[0]
                        +"?page="+pathVariableArray[1]+"&size="+pathVariableArray[2]+"&includeCount="+pathVariableArray[3]+"&id="+pathVariableArray[4]
                        +"&status="+pathVariableArray[5]+"&firstName="+pathVariableArray[6]+"&lastName="+pathVariableArray[7]
                        +"&email="+pathVariableArray[8]+"&city="+pathVariableArray[9]+"&stateProvince="+pathVariableArray[10]
                        +"&postalCode="+pathVariableArray[11]+"&country="+pathVariableArray[12]+"&properties="+pathVariableArray[13]
                        +"&trustLevel="+pathVariableArray[14]+"&block="+pathVariableArray[15];
                break;
            case "customers/":
                requestPath = basicPath+pathVariableArray[0]
                        +pathVariableArray[1];
                break;
            case "dailytransactionreport":
                requestPath = basicPath+"reports/"+pathVariableArray[0]+"?enddate="+pathVariableArray[1]+"&startdate="+pathVariableArray[2]+"&type="+pathVariableArray[3];
                break;
            case "tax":
                requestPath = basicPath+pathVariableArray[0];
                break;
            case "systemalert":
                requestPath = basicPath+pathVariableArray[0]
                        +"?iscurrent="+pathVariableArray[1];
                break;
            case "versions":
                requestPath = basicPath+pathVariableArray[0];
                break;
            case "healthcheck/detail":
                requestPath = basicPath+pathVariableArray[0];
                break;
            case "configuration/secrets":
                requestPath = basicPath+pathVariableArray[0];
                break;
            case "configuration/secrets/":
                requestPath = basicPath+pathVariableArray[0]
                        +pathVariableArray[1];
                break;
            case "reports/payoutreports":
                requestPath = basicPath+pathVariableArray[0];
                break;
            case "reports/payoutreports/":
                requestPath = basicPath+pathVariableArray[0]
                        +pathVariableArray[1];
                break;
            case "reports/subscriptionreport":
                requestPath = basicPath + pathVariableArray[0] + "?enddate="+pathVariableArray[1]+"&startdate="+pathVariableArray[2]+"&type="+pathVariableArray[3];
                break;
            case "customers/id/financialsummaries":
                requestPath = basicPath+"customers/"
                        +pathVariableArray[1]+"/financialsummaries";
                break;
            case "callers":
                requestPath = basicPath+pathVariableArray[0];
                break;
            case "callers/":
                requestPath = basicPath+pathVariableArray[0]
                        +pathVariableArray[1];
                break;
            case "callers/passwords":
                requestPath = basicPath+"callers/"
                        +pathVariableArray[1]+"/passwords";
                break;
            case "post /charges":
                requestPath = basicPath+"charges";
                break;
            case "post /charges/id":
                requestPath = basicPath+"charges/"+pathVariableArray[1];
                break;
            case "charges/verification":
                requestPath = basicPath+pathVariableArray[0];
                break;
            case "charges/token":
                requestPath = basicPath+pathVariableArray[0]+"?txnRef="+pathVariableArray[1]+"&token="+pathVariableArray[2]+"&PayerID="+pathVariableArray[3];
                break;
            case "post paymentprofiles":
                requestPath = basicPath+"paymentprofiles";
                break;
            case "put systemalert":
                requestPath = basicPath+"systemalert";
                break;
            case "put callers/":
                requestPath = basicPath+"callers/"+ pathVariableArray[1];
                break;
            case "put callers/callerName/passwords/tag":
                requestPath = basicPath + "callers/" + pathVariableArray[1]+"/passwords/"+pathVariableArray[2];
                break;
            case "post paymentprofiles/id":
                requestPath = basicPath+"paymentprofiles/"+pathVariableArray[1];
                break;
            case "delete paymentprofiles/id":
                requestPath = basicPath+"paymentprofiles/"+pathVariableArray[1];
                break;
            case "post customers":
                requestPath = basicPath+"customers";
                break;
            case "post customers/id":
                requestPath = basicPath+"customers/"+pathVariableArray[1];
                break;
            case "post callers":
                requestPath = basicPath+"callers";
                break;
            case "delete callers/callerName":
                requestPath = basicPath+"callers/"+pathVariableArray[1];
                break;
            case "post callers/password":
                requestPath = basicPath+"callers/"+pathVariableArray[1]+"/passwords";
                break;
            case "post callers/unlock":
                requestPath = basicPath+"callers/"+pathVariableArray[1]+"/unlock";
                break;
            case "delete callers/callerName/passwords/tag":
                requestPath = basicPath + "callers/" + pathVariableArray[1]+"/passwords/"+pathVariableArray[2];
                break;
            case "post test/notifications":
                requestPath = basicPath + "test/notifications";
                break;
            case "post accountprovisioning":
                requestPath = basicPath + "accountprovisioning";
                break;
            case "post configuration":
                requestPath = basicPath + "configuration";
                break;
            case "post configuration/secrets/secretKeyName":
                requestPath = basicPath + "configuration/secrets/" + pathVariableArray[1];
                break;
            case "verify missing required fields post /charges":
                requestPath = basicPath + "charges";
                break;
            default:
                break;
        }

        return requestPath;
    }

    public static String generateHmacSignatureHex(String callerName, String merchantAccount, String timestamp, String secretKey,
                                                  String requestPath, String requestBody) throws Exception{
        Mac mac = Mac.getInstance(HMAC_SHA256_ALGORITHM);
        SecretKeySpec keySpec = new SecretKeySpec(secretKey.getBytes(), HMAC_SHA256_ALGORITHM);

        mac.init(keySpec);

        StringBuilder message = new StringBuilder().append(callerName).append(merchantAccount).append(timestamp);

        if (requestPath != null && requestPath.trim().length() > 0) {
            message.append(requestPath);
        }
        if (requestBody != null && requestBody.trim().length() > 0) {
            message.append(requestBody);
        }

        byte[] signatureBytes = mac.doFinal(message.toString().getBytes());
        String signature = DatatypeConverter.printHexBinary(signatureBytes);

        return signature;

    }
}

