import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(monochrome = false, format={"pretty", "json:target/cucumber.json", "html:reports/TEST_REPORTS.json"},
        features = {"src/test/resources/0666_TEST"}, tags = {"@testscenario"})

public class V3_APICukeTest_TEST {

}
