package step_definitions;

import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import utils.Utilities;

/**
 * Created by rbedino on 1/11/2016
 */

public class APIStepDefs_PUBLIC {

    public WebDriver driver = Hooks.driver;
    public String apiName;


    public String requestPath;
    public String requestBody;
    public String timestamp;

    String sppId;
    String customerId;


    @When("^I try to check the GET Healthcheck responses with '(.+)' credentials '(.+)', '(.+)', '(.+)', '(.+)'$")
    public void get_healthcheck(String validationType, String merchantAccount, String callerName, String secretKey, String onBehalfOf) throws Exception {
        apiName = "healthcheck";

        if (merchantAccount.equals("*")) merchantAccount = ""; if (callerName.equals("*")) callerName = ""; if (secretKey.equals("*")) secretKey = "";
        if (onBehalfOf.equals("*")) onBehalfOf = "";

        this.requestBody = "";

        String[] pathVariableArray = new String[]{apiName};
        this.requestPath = Utilities.getRequestPath(pathVariableArray);

        this.timestamp = Utilities.getTimestamp();

        String signature = Utilities.generateHmacSignatureHex(callerName,merchantAccount,timestamp,secretKey,requestPath,requestBody);

        String[] requestArray = new String[]{validationType,callerName,merchantAccount,timestamp,signature,onBehalfOf,requestBody,requestPath};

        APIMethods.GETHealthcheck(requestArray);
    }


    @When("^I try to check the POST /charges responses with '(.+)' '(.+)' credentials '(.+)', '(.+)', '(.+)', '(.+)'$")
    public void POST_charges(String validationType, String pymtType, String merchantAccount, String callerName, String secretKey, String onBehalfOf) throws Exception {
        apiName = "post /charges";

        if (validationType.equals("oneOff")){
            switch(pymtType){
                case "credit card":
                    merchantAccount="SQAUTOMATION";
                    callerName="$apiTest";
                    secretKey="Secret12";
                    onBehalfOf="";
                    break;
                case "prepaid card":
                    callerName="$apiTest";
                    secretKey="Secret12";
                    onBehalfOf="";
                    break;
                case "paypal":
                    callerName="$apiTest";
                    secretKey="Secret12";
                    onBehalfOf="";
                    break;
                case "check":
                    callerName="$apiTest";
                    secretKey="Secret12";
                    onBehalfOf="";
                    break;
                case "recorder":
                    callerName="$apiTest";
                    secretKey="Secret12";
                    onBehalfOf="";
                    break;
                default:
                    callerName="$apiTest";
                    secretKey="Secret12";
                    onBehalfOf="";
                    break;
            }
            validationType = "valid";
        }

        if (validationType.equals("sppType")){
            switch(pymtType){
                default:
                    callerName="$apiTest";
                    secretKey="Secret12";
                    onBehalfOf="";
                    sppId=APIMethods.sppId;
                    break;
            }
            validationType = "valid";
        }

        else {
            if (merchantAccount.equals("*")) merchantAccount = ""; if (callerName.equals("*")) callerName = ""; if (secretKey.equals("*")) secretKey = "";
            if (onBehalfOf.equals("*")) onBehalfOf = "";

            if (pymtType.equals("*")) pymtType = "";
            if (pymtType.equals("SPP")) {
                sppId = APIMethods.sppId;
                customerId = APIMethods.customerId;
            }
            if (validationType.equals("param")) sppId="6666666";
        }

        String[] requiredFields = new String[]{pymtType, sppId, customerId};
        this.requestBody = Utilities.getRequestBody(apiName, validationType, requiredFields);

        String[] pathVariableArray = new String[]{apiName};
        this.requestPath = Utilities.getRequestPath(pathVariableArray);

        this.timestamp = Utilities.getTimestamp();
        String signature = Utilities.generateHmacSignatureHex(callerName,merchantAccount,timestamp,secretKey,requestPath,requestBody);

        String[] requestArray = new String[]{validationType,callerName,merchantAccount,timestamp,signature,onBehalfOf,requestBody,requestPath,pymtType};

        APIMethods.POSTCharges(requestArray);
    }



    @When("^I try to check the POST /paymentprofiles responses with '(.+)' '(.+)' credentials '(.+)', '(.+)', '(.+)', '(.+)'$")
    public void POST_payment_profile(String validationType, String profileType, String merchantAccount, String callerName, String secretKey, String onBehalfOf) throws Exception {
        this.apiName = "post paymentprofiles";

        if (validationType.equals("oneOff")){
            switch(profileType){
                case "credit card":
                    merchantAccount="SQAUTOMATION";
                    callerName="$apiTest";
                    secretKey="Secret12";
                    onBehalfOf="";
                    break;
                case "paypal":
                    merchantAccount="SQAUTOMATION";
                    callerName="$apiTest";
                    secretKey="Secret12";
                    onBehalfOf="";
                    break;
                case "check":
                    merchantAccount="SQAUTOMATION";
                    callerName="$apiTest";
                    secretKey="Secret12";
                    onBehalfOf="";
                    break;
                case "recorder":
                    merchantAccount="SQAUTOMATION";
                    callerName="$apiTest";
                    secretKey="Secret12";
                    onBehalfOf="";
                    break;
                default:
                    merchantAccount="SQAUTOMATION";
                    callerName="$apiTest";
                    secretKey="Secret12";
                    onBehalfOf="";
                    break;
            }
            validationType = "valid";
        }

        else {
            if (merchantAccount.equals("*")) merchantAccount = ""; if (callerName.equals("*")) callerName = ""; if (secretKey.equals("*")) secretKey = "";
            if (onBehalfOf.equals("*")) onBehalfOf = "";

            if (profileType.equals("*")) profileType = "";
        }

        String[] requiredFields = new String[]{profileType, APIMethods.customerId};
        this.requestBody = Utilities.getRequestBody(apiName, validationType, requiredFields);

        String[] pathVariableArray = new String[]{apiName};
        this.requestPath = Utilities.getRequestPath(pathVariableArray);

        this.timestamp = Utilities.getTimestamp();
        String signature = Utilities.generateHmacSignatureHex(callerName,merchantAccount,timestamp,secretKey,requestPath,requestBody);

        String[] requestArray = new String[]{validationType,callerName,merchantAccount,timestamp,signature,onBehalfOf,requestBody,requestPath};

        APIMethods.POSTPymtProfile(requestArray);
    }


    @When("^I try to check the DELETE /paymentprofiles/id responses with '(.+)' '(.+)' credentials '(.+)', '(.+)', '(.+)', '(.+)', '(.+)'$")
    public void DELETE_payment_profile_id(String validationType, String profileType, String merchantAccount, String callerName, String secretKey, String onBehalfOf,
                                        String sppId) throws Exception {
        apiName = "delete paymentprofiles/id";

        if (merchantAccount.equals("*")) merchantAccount = ""; if (callerName.equals("*")) callerName = ""; if (secretKey.equals("*")) secretKey = "";
        if (onBehalfOf.equals("*")) onBehalfOf = "";

        if (profileType.equals("*")) profileType = "";

        if (sppId.equals("*")) sppId = APIMethods.sppId;

        this.requestBody = "";

        String[] pathVariableArray = new String[]{apiName, sppId};
        this.requestPath = Utilities.getRequestPath(pathVariableArray);

        this.timestamp = Utilities.getTimestamp();
        String signature = Utilities.generateHmacSignatureHex(callerName,merchantAccount,timestamp,secretKey,requestPath,requestBody);

        String[] requestArray = new String[]{validationType,callerName,merchantAccount,timestamp,signature,onBehalfOf,requestBody,requestPath};

        APIMethods.DELETEPymtProfileId(requestArray);
    }


    @When("^I try to check the POST /customers responses with '(.+)' credentials '(.+)', '(.+)', '(.+)', '(.+)'$")
    public void POST_customers(String validationType, String merchantAccount, String callerName, String secretKey, String onBehalfOf) throws Exception {
        apiName = "post customers";

        if (validationType.equals("oneOff")){
            merchantAccount="SQAUTOMATION";
            callerName="$apiTest";
            secretKey="Secret12";
            onBehalfOf="";

            validationType = "valid";
        }

        else {
            if (merchantAccount.equals("*")) merchantAccount = ""; if (callerName.equals("*")) callerName = ""; if (secretKey.equals("*")) secretKey = "";
            if (onBehalfOf.equals("*")) onBehalfOf = "";
        }

        String[] requiredFields = new String[]{};
        this.requestBody = Utilities.getRequestBody(apiName, validationType, requiredFields);

        String[] pathVariableArray = new String[]{apiName};
        this.requestPath = Utilities.getRequestPath(pathVariableArray);

        this.timestamp = Utilities.getTimestamp();
        String signature = Utilities.generateHmacSignatureHex(callerName,merchantAccount,timestamp,secretKey,requestPath,requestBody);

        String[] requestArray = new String[]{validationType,callerName,merchantAccount,timestamp,signature,onBehalfOf,requestBody,requestPath};

        APIMethods.POSTCustomers(requestArray);
    }

}