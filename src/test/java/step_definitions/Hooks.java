package step_definitions;

import cucumber.api.java.Before;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

public class Hooks {
    protected static RemoteWebDriver driver;

    public static String v3Properties = "src/test/java/properties/v3_api.properties";

    public static String browserStack;

    public static String testRailURL;
    public static String testRailLogin;
    public static String testRailPass;
    public static String testRailProject;
    public static String testRailPlan;

    public static String testCasePurpose;

    public static String apiUrl;

    public static String superadminMerchant;
    public static String superadminCaller;
    public static String superadminCred;

    public static String browserConfig;

    public static String validTaxData="src/test/java/utils/testdata/tax/01validTaxData.json";
    public static String invalidTaxData="src/test/java/utils/testdata/tax/02invalidTaxData.json";
    public static String paramTaxData="src/test/java/utils/testdata/tax/03paramTaxData.json";

    public static String chargesPost="src/test/java/utils/testdata/charges/chargesPost.json";
    public static String postCharges_PREPAID="src/test/java/utils/testdata/charges/postCharges_PREPAID.json";
    public static String postCharges_CHECK="src/test/java/utils/testdata/charges/postCharges_CHECK.json";
    public static String postCharges_RECORDER="src/test/java/utils/testdata/charges/postCharges_RECORDER.json";
    public static String postCharges_SPP="src/test/java/utils/testdata/charges/postCharges_SPP.json";

    public static String missingFieldCreditCard="src/test/java/utils/testdata/charges/missingFieldCreditCard.json";
    public static String paramRequiredField_CreditCard="src/test/java/utils/testdata/charges/ParamRequiredField_CreditCard.json";

    public static String validChargesData="src/test/java/utils/testdata/charges/01validChargesData.json";
    public static String validChargesData_AltaPay_US_MasterCard="src/test/java/utils/testdata/chargeValidations_V3/validChargesData_AltaPay_US_MasterCard.json";
    public static String validChargesData_AltaPay_US_Visa="src/test/java/utils/testdata/chargeValidations_V3/validChargesData_AltaPay_US_Visa.json";
    public static String validChargesData_AltaPay_US_American_Express="src/test/java/utils/testdata/chargeValidations_V3/validChargesData_AltaPay_US_American_Express.json";
    public static String validChargesData_AltaPay_US_Diners="src/test/java/utils/testdata/chargeValidations_V3/validChargesData_AltaPay_US_Diners.json";
    public static String validChargesData_AltaPay_US_Maestro = "src/test/java/utils/testdata/chargeValidations_V3/validChargesData_AltaPay_US_Maestro.json";
    public static String validChargesData_AltaPay_US_JCB = "src/test/java/utils/testdata/chargeValidations_V3/validChargesData_AltaPay_US_JCB.json";
    public static String validChargesData_AltaPay_US_Discover = "src/test/java/utils/testdata/chargeValidations_V3/validChargesData_AltaPay_US_Discover.json";
    public static String validChargesData_Allpago_BR_MasterCard = "src/test/java/utils/testdata/chargeValidations_V3/validChargesData_Allpago_BR_MasterCard.json";
    public static String validChargesData_Allpago_MX_MasterCard = "src/test/java/utils/testdata/chargeValidations_V3/validChargesData_Allpago_MX_MasterCard.json";
    public static String validChargesData_Allpago_BR_Visa = "src/test/java/utils/testdata/chargeValidations_V3/validChargesData_Allpago_BR_Visa.json";
    public static String validChargesData_Allpago_MX_Visa = "src/test/java/utils/testdata/chargeValidations_V3/validChargesData_Allpago_MX_Visa.json";
    public static String validChargesData_Allpago_BR_Boleto = "src/test/java/utils/testdata/chargeValidations_V3/validChargesData_Allpago_BR_Boleto.json";
    public static String validChargesData_Allpago_MX_OXXO = "src/test/java/utils/testdata/chargeValidations_V3/validChargesData_Allpago_MX_OXXO.json";
    public static String validChargesData_Astropay_BR_Bradesco = "src/test/java/utils/testdata/chargeValidations_V3/validChargesData_Astropay_BR_Bradesco.json";
    public static String validChargesData_Astropay_BR_Banco = "src/test/java/utils/testdata/chargeValidations_V3/validChargesData_Astropay_BR_Banco.json";
    public static String validChargesData_Astropay_BR_HSBC = "src/test/java/utils/testdata/chargeValidations_V3/validChargesData_Astropay_BR_HSBC.json";
    public static String validChargesData_Astropay_MX_Bancomer = "src/test/java/utils/testdata/chargeValidations_V3/validChargesData_Astropay_MX_Bancomer.json";
    public static String validChargesData_Astropay_MX_Santander = "src/test/java/utils/testdata/chargeValidations_V3/validChargesData_Astropay_MX_Santander.json";
    public static String validChargesData_Astropay_MX_Banamex = "src/test/java/utils/testdata/chargeValidations_V3/validChargesData_Astropay_MX_Banamex.json";
    public static String validChargesData_Astropay_BR_Itau = "src/test/java/utils/testdata/chargeValidations_V3/validChargesData_Astropay_BR_Itau.json";


    public static String validChargesData_PAYPAL="src/test/java/utils/testdata/charges/01validChargesData_PAYPAL.json";
    public static String validOnBehalfChargesData="src/test/java/utils/testdata/charges/02validOnBehalfChargesData.json";
    public static String paramChargesData="src/test/java/utils/testdata/charges/03paramChargesData.json";

    public static String validUpdateChargesData="src/test/java/utils/testdata/charges/04validUpdateChargesData.json";
    public static String paramUpdateChargesData="src/test/java/utils/testdata/charges/05paramUpdateChargesData.json";

    public static String validVerifyChargesData="src/test/java/utils/testdata/charges/06validVerifyChargesData.json";
    public static String paramVerifyChargesData="src/test/java/utils/testdata/charges/07paramVerifyChargesData.json";

    public static String validCCPaymentProfile="src/test/java/utils/testdata/paymentprofiles/01validCCPaymentProfile.json";
    public static String invalidCCPaymentProfile="src/test/java/utils/testdata/paymentprofiles/02invalidCCPaymentProfile.json";
    public static String updateCCPaymentProfile="src/test/java/utils/testdata/paymentprofiles/03updateCCPaymentProfile.json";
    public static String paramCCPaymentProfile="src/test/java/utils/testdata/paymentprofiles/04paramCCPaymentProfile.json";

    public static String systemAlertData="src/test/java/utils/testdata/alert/alert.json";
    public static String callerNameData="src/test/java/utils/testdata/callers/callerNameData.json";
    public static String updateCallerNamePassword="src/test/java/utils/testdata/callers/PUTCallerNamePassword.json";

    public static String validCCPaymentProfilePOST="src/test/java/utils/testdata/paymentprofiles/05validCCPaymentProfilePOST.json";

    public static String validCustomer="src/test/java/utils/testdata/customers/01validCustomer.json";
    public static String invalidCustomer="src/test/java/utils/testdata/customers/02invalidCustomer.json";
    public static String paramCustomer="src/test/java/utils/testdata/customers/03paramCustomer.json";

    public static String validCustomerId="src/test/java/utils/testdata/customers/04validCustomerId.json";

    public static String postCallerData="src/test/java/utils/testdata/callers/POSTCallerData.json";

    public static String postNotification="src/test/java/utils/testdata/notifications/postNotification.json";


    public static String postAccountProvisioning="src/test/java/utils/testdata/accountProvisioning/postAccountProvisioning.json";
    public static String paramPostAccountProvisioning="src/test/java/utils/testdata/accountProvisioning/paramPostAccountProvisioning.json";

    public static String postConfiguration="src/test/java/utils/testdata/configuration/postConfiguration.json";
    public static String paramPostConfiguration="src/test/java/utils/testdata/configuration/paramPostConfiguration.json";

    public static String postConfigurationSecretKey="src/test/java/utils/testdata/secretkeys/postConfigurationSecretKey.json";
    public static String invalidPostConfigurationSecretKey="src/test/java/utils/testdata/secretkeys/invalidPostConfigurationSecretKey.json";


    @Before
    public void setUp() throws Exception {
        InputStream inGlobal = new FileInputStream(v3Properties);
        Properties propGlobal = new Properties();
        propGlobal.load(inGlobal);

        browserStack=propGlobal.getProperty("browserStack");

        testRailURL=propGlobal.getProperty("testrail_url");
        testRailLogin=propGlobal.getProperty("testrail_login");
        testRailPass=propGlobal.getProperty("testrail_pass");
        testRailProject=propGlobal.getProperty("testrail_project");
        testRailPlan=propGlobal.getProperty("testrail_plan");

        String purpose=propGlobal.getProperty("test_purpose");
        testCasePurpose=System.getProperty("test.purpose", purpose);

        String url=propGlobal.getProperty("apiUrl");
        apiUrl="http://"+System.getProperty("api.url", url);

        superadminMerchant=propGlobal.getProperty("superadmin.merchant");
        superadminCaller=propGlobal.getProperty("superadmin.caller");
        superadminCred=propGlobal.getProperty("superadmin.cred");

        inGlobal.close();

        browserConfig="headless";
    }
}
