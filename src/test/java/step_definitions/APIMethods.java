package step_definitions;

import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.response.Response;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

/**
 * Created by rbedino on 1/11/2016
 */

    // Status Key for method names
    // 200 - 'valid'
    // 400 - 'invalid'
    // 404 - 'invalidSuper'
    // 403 - 'param'
    // 401 - 'access'

//  String body = res.getBody().asString();
//  System.out.println(body);


public class APIMethods {

    public static String chargeId;
    public static String customerId;
    public static String sppId;
    public static String txnRef;
    public static String token;
    public static String PayerID;
    public static String custId;
//    public static String callerUserName;

    // Method for printing header values
    public static void PrintHeaderValues(String [] requestArray){
        System.out.println("VALIDATION TYPE - "+requestArray[0]);
        System.out.println("CALLER NAME - "+requestArray[1]);
        System.out.println("MERCHANT - "+requestArray[2]);
        System.out.println("TIMESTAMP - "+requestArray[3]);
        System.out.println("HMAC SIGNATURE - "+requestArray[4]);
        System.out.println("ON BEHALF OF - "+requestArray[5]);
        System.out.println("REQUEST BODY - "+requestArray[6]);
        System.out.println("REQUEST PATH - "+requestArray[7]);

        System.out.println("REQUEST URL = "+Hooks.apiUrl);
        System.out.println("TEST PURPOSE = "+Hooks.testCasePurpose);
    }

// TODO PUBLIC API METHODS

// TODO MONITORING ENDPOINT

// TODO GET Healthcheck
    // Method : V3 API GET Healthcheck Response
    public static void GETHealthcheck(String[] requestArray){
        PrintHeaderValues(requestArray);

        switch(requestArray[0]){
            case "valid":
                given().
                        contentType(ContentType.JSON).
                        headers("X-Live-Gamer-CallerName", requestArray[1], "X-Live-Gamer-MerchantAccount", requestArray[2],
                                "X-Live-Gamer-HMAC-Timestamp", requestArray[3], "X-Live-Gamer-HMAC-Signature", requestArray[4],
                                "X-Live-Gamer-OnBehalfOf", requestArray[5]).
                        and().
                        when().
                        get(Hooks.apiUrl + requestArray[7]).
                        then().
                        statusCode(200);
                break;
            case "access":
                given().
                        contentType(ContentType.JSON).
                        headers("X-Live-Gamer-CallerName", requestArray[1], "X-Live-Gamer-MerchantAccount", requestArray[2],
                                "X-Live-Gamer-HMAC-Timestamp", requestArray[3], "X-Live-Gamer-HMAC-Signature", requestArray[4],
                                "X-Live-Gamer-OnBehalfOf", requestArray[5]).
                        and().
                        when().
                        get(Hooks.apiUrl + requestArray[7]).
                        then().
                        statusCode(401);
                break;
            default:
                break;
        }
    }


// TODO POST Charges
    // Method : V3 API POST Charges Response
    public static String POSTCharges(String[] requestArray) throws UnsupportedEncodingException {
//        System.out.println("Request URL = "+Hooks.apiUrl + requestArray[7]);
        PrintHeaderValues(requestArray);
        switch(requestArray[0]){
            case "valid":
                Response res = given().
                        contentType(ContentType.JSON).
                        accept(ContentType.JSON).
                        headers("X-Live-Gamer-CallerName", requestArray[1], "X-Live-Gamer-MerchantAccount", requestArray[2],
                                "X-Live-Gamer-HMAC-Timestamp", requestArray[3], "X-Live-Gamer-HMAC-Signature", requestArray[4],
                                "X-Live-Gamer-OnBehalfOf", requestArray[5]).
                        body(requestArray[6]).
                        expect().
                        statusCode(201).
                        when().
                        post(Hooks.apiUrl + requestArray[7]);

                JsonPath jp = new JsonPath(res.asString());
                chargeId = jp.get("id").toString();
                customerId = jp.get("customer.id").toString();

                System.out.println("Charge Id is "+chargeId);
                System.out.println("Customer Id is "+customerId);
                System.out.println("\n");

                switch(requestArray[8]){
                    case "credit card":
                        assertThat(jp.get("transactions.state").toString(), equalTo("[DECLINED]"));
                        break;
                    case "paypal":
                        token = jp.get("transactions.paymentServiceURL").toString().substring(48, 68);
                        txnRef = jp.get("transactions.transactionRef").toString().substring(1, 85);
                        String txnOrig = jp.get("transactions.transactionRef").toString().substring(1, 83);
                        txnRef = URLEncoder.encode(txnOrig, "UTF-8");
                        PayerID = "";
                        break;
                    default:
                        break;
                }
                break;
            case "param":
                res = given().
                        contentType(ContentType.JSON).
                        accept(ContentType.JSON).
                        headers("X-Live-Gamer-CallerName", requestArray[1], "X-Live-Gamer-MerchantAccount", requestArray[2],
                                "X-Live-Gamer-HMAC-Timestamp", requestArray[3], "X-Live-Gamer-HMAC-Signature", requestArray[4],
                                "X-Live-Gamer-OnBehalfOf", requestArray[5]).
                        body(requestArray[6]).
                        expect().
                        statusCode(400).
                        when().
                        post(Hooks.apiUrl + requestArray[7]);

                jp = new JsonPath(res.asString());
                chargeId = jp.get("id").toString();
                customerId = jp.get("customer.id").toString();

                System.out.println(chargeId);
                System.out.println(customerId);
                System.out.println("\n");

                switch(requestArray[8]){
                    case "credit card":
                        assertThat(jp.get("transactions.state").toString(), equalTo("[DECLINED]"));
                        assertThat(jp.get("charge").toString(), equalTo("CHARGED"));
                        break;
                    case "paypal":
                        token = jp.get("transactions.paymentServiceURL").toString().substring(48, 68);
                        txnRef = jp.get("transactions.transactionRef").toString().substring(1, 85);
                        String txnOrig = jp.get("transactions.transactionRef").toString().substring(1, 83);
                        txnRef = URLEncoder.encode(txnOrig, "UTF-8");
                        PayerID = "";
                        break;
                    default:
                        break;
                }
                break;
            case "access":
                given().
                        contentType(ContentType.JSON).
                        accept(ContentType.JSON).
                        headers("X-Live-Gamer-CallerName", requestArray[1], "X-Live-Gamer-MerchantAccount", requestArray[2],
                                "X-Live-Gamer-HMAC-Timestamp", requestArray[3], "X-Live-Gamer-HMAC-Signature", requestArray[4],
                                "X-Live-Gamer-OnBehalfOf", requestArray[5]).
                        body(requestArray[6]).
                        and().
                        when().
                        post(Hooks.apiUrl + requestArray[7]).
                        then().
                        statusCode(401);
                break;
            default:
                break;
        }
        return chargeId;
    }


// TODO POST Payment Profiles
    // Method : V3 API POST PymtProfile Response
    public static void POSTPymtProfile(String[] requestArray){
        PrintHeaderValues(requestArray);

        switch(requestArray[0]){
            case "valid":
                Response res = given().
                        contentType(ContentType.JSON).
                        accept(ContentType.JSON).
                        headers("X-Live-Gamer-CallerName", requestArray[1], "X-Live-Gamer-MerchantAccount", requestArray[2],
                                "X-Live-Gamer-HMAC-Timestamp", requestArray[3], "X-Live-Gamer-HMAC-Signature", requestArray[4],
                                "X-Live-Gamer-OnBehalfOf", requestArray[5]).
                        body(requestArray[6]).
                        expect().
                        statusCode(201).
                        when().
                        post(Hooks.apiUrl + requestArray[7]);

                JsonPath jp = new JsonPath(res.asString());

                break;
            case "invalid":
                given().
                        contentType(ContentType.JSON).
                        accept(ContentType.JSON).
                        headers("X-Live-Gamer-CallerName", requestArray[1], "X-Live-Gamer-MerchantAccount", requestArray[2],
                                "X-Live-Gamer-HMAC-Timestamp", requestArray[3], "X-Live-Gamer-HMAC-Signature", requestArray[4],
                                "X-Live-Gamer-OnBehalfOf", requestArray[5]).
                        body(requestArray[6]).
                        and().
                        when().
                        post(Hooks.apiUrl + requestArray[7]).
                        then().
                        statusCode(400);
                break;
            case "param":
                given().
                        contentType(ContentType.JSON).
                        accept(ContentType.JSON).
                        headers("X-Live-Gamer-CallerName", requestArray[1], "X-Live-Gamer-MerchantAccount", requestArray[2],
                                "X-Live-Gamer-HMAC-Timestamp", requestArray[3], "X-Live-Gamer-HMAC-Signature", requestArray[4],
                                "X-Live-Gamer-OnBehalfOf", requestArray[5]).
                        body(requestArray[6]).
                        and().
                        when().
                        post(Hooks.apiUrl + requestArray[7]).
                        then().
                        statusCode(404);
                break;
            case "access":
                given().
                        contentType(ContentType.JSON).
                        accept(ContentType.JSON).
                        headers("X-Live-Gamer-CallerName", requestArray[1], "X-Live-Gamer-MerchantAccount", requestArray[2],
                                "X-Live-Gamer-HMAC-Timestamp", requestArray[3], "X-Live-Gamer-HMAC-Signature", requestArray[4],
                                "X-Live-Gamer-OnBehalfOf", requestArray[5]).
                        body(requestArray[6]).
                        and().
                        when().
                        post(Hooks.apiUrl + requestArray[7]).
                        then().
                        statusCode(401);
                break;
            default:
                break;
        }
    }


// TODO DELETE Payment Profiles by Id
    // Method : V3 API DELETE PymtProfileId Response
    public static void DELETEPymtProfileId(String[] requestArray){
        PrintHeaderValues(requestArray);

        switch(requestArray[0]){
            case "valid":
                given().
                        contentType(ContentType.JSON).
                        accept(ContentType.JSON).
                        headers("X-Live-Gamer-CallerName", requestArray[1], "X-Live-Gamer-MerchantAccount", requestArray[2],
                                "X-Live-Gamer-HMAC-Timestamp", requestArray[3], "X-Live-Gamer-HMAC-Signature", requestArray[4],
                                "X-Live-Gamer-OnBehalfOf", requestArray[5]).
                        body(requestArray[6]).
                        and().
                        when().
                        delete(Hooks.apiUrl + requestArray[7]).
                        then().
                        statusCode(204);
                break;
            case "invalid":
                given().
                        contentType(ContentType.JSON).
                        accept(ContentType.JSON).
                        headers("X-Live-Gamer-CallerName", requestArray[1], "X-Live-Gamer-MerchantAccount", requestArray[2],
                                "X-Live-Gamer-HMAC-Timestamp", requestArray[3], "X-Live-Gamer-HMAC-Signature", requestArray[4],
                                "X-Live-Gamer-OnBehalfOf", requestArray[5]).
                        body(requestArray[6]).
                        and().
                        when().
                        delete(Hooks.apiUrl + requestArray[7]).
                        then().
                        statusCode(400);
                break;
            case "param":
                given().
                        contentType(ContentType.JSON).
                        accept(ContentType.JSON).
                        headers("X-Live-Gamer-CallerName", requestArray[1], "X-Live-Gamer-MerchantAccount", requestArray[2],
                                "X-Live-Gamer-HMAC-Timestamp", requestArray[3], "X-Live-Gamer-HMAC-Signature", requestArray[4],
                                "X-Live-Gamer-OnBehalfOf", requestArray[5]).
                        body(requestArray[6]).
                        and().
                        when().
                        delete(Hooks.apiUrl + requestArray[7]).
                        then().
                        statusCode(404);
                break;
            case "access":
                given().
                        contentType(ContentType.JSON).
                        accept(ContentType.JSON).
                        headers("X-Live-Gamer-CallerName", requestArray[1], "X-Live-Gamer-MerchantAccount", requestArray[2],
                                "X-Live-Gamer-HMAC-Timestamp", requestArray[3], "X-Live-Gamer-HMAC-Signature", requestArray[4],
                                "X-Live-Gamer-OnBehalfOf", requestArray[5]).
                        body(requestArray[6]).
                        and().
                        when().
                        delete(Hooks.apiUrl + requestArray[7]).
                        then().
                        statusCode(401);
                break;
            default:
                break;
        }
    }


// TODO POST Customers
    // Method : V3 API POST Customer Response
    public static void POSTCustomers(String[] requestArray){
        PrintHeaderValues(requestArray);

        switch(requestArray[0]){
            case "valid":
                Response res = given().
                        contentType(ContentType.JSON).
                        accept(ContentType.JSON).
                        headers("X-Live-Gamer-CallerName", requestArray[1], "X-Live-Gamer-MerchantAccount", requestArray[2],
                                "X-Live-Gamer-HMAC-Timestamp", requestArray[3], "X-Live-Gamer-HMAC-Signature", requestArray[4],
                                "X-Live-Gamer-OnBehalfOf", requestArray[5]).
                        body(requestArray[6]).
                        expect().
                        statusCode(201).
                        when().
                        post(Hooks.apiUrl + requestArray[7]);

                JsonPath jp = new JsonPath(res.asString());
                custId = jp.get("id").toString();

                break;
            case "invalid":
                given().
                        contentType(ContentType.JSON).
                        accept(ContentType.JSON).
                        headers("X-Live-Gamer-CallerName", requestArray[1], "X-Live-Gamer-MerchantAccount", requestArray[2],
                                "X-Live-Gamer-HMAC-Timestamp", requestArray[3], "X-Live-Gamer-HMAC-Signature", requestArray[4],
                                "X-Live-Gamer-OnBehalfOf", requestArray[5]).
                        body(requestArray[6]).
                        and().
                        when().
                        post(Hooks.apiUrl + requestArray[7]).
                        then().
                        statusCode(400);
                break;
            case "param":
                given().
                        contentType(ContentType.JSON).
                        accept(ContentType.JSON).
                        headers("X-Live-Gamer-CallerName", requestArray[1], "X-Live-Gamer-MerchantAccount", requestArray[2],
                                "X-Live-Gamer-HMAC-Timestamp", requestArray[3], "X-Live-Gamer-HMAC-Signature", requestArray[4],
                                "X-Live-Gamer-OnBehalfOf", requestArray[5]).
                        body(requestArray[6]).
                        and().
                        when().
                        post(Hooks.apiUrl + requestArray[7]).
                        then().
                        statusCode(409);
                break;
            case "access":
                given().
                        contentType(ContentType.JSON).
                        accept(ContentType.JSON).
                        headers("X-Live-Gamer-CallerName", requestArray[1], "X-Live-Gamer-MerchantAccount", requestArray[2],
                                "X-Live-Gamer-HMAC-Timestamp", requestArray[3], "X-Live-Gamer-HMAC-Signature", requestArray[4],
                                "X-Live-Gamer-OnBehalfOf", requestArray[5]).
                        body(requestArray[6]).
                        and().
                        when().
                        post(Hooks.apiUrl + requestArray[7]).
                        then().
                        statusCode(401);
                break;
            default:
                break;
        }
    }
}
