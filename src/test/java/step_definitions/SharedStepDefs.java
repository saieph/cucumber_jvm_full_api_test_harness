package step_definitions;

import com.rmn.testrail.entity.Project;
import com.rmn.testrail.entity.TestPlan;
import com.rmn.testrail.entity.TestResult;
import com.rmn.testrail.service.TestRailService;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import helpers.testrail.APIException;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.WebDriver;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import static step_definitions.Hooks.apiUrl;

public class SharedStepDefs {

// initializes session variables
    public Integer testId;
    public static String testPurpose;
    public WebDriver driver = Hooks.driver;


    public static String getRandomCust() {
        String randomCust = RandomStringUtils.randomAlphabetic(5)+"CUST";
        return randomCust;
    }

    public static String getRandomTxnId() {
        String randomTxnId = RandomStringUtils.randomAlphabetic(5) + "TXN";
        return randomTxnId;
    }

    public static String getRandomEmail() {
        String randomEmail = RandomStringUtils.randomAlphabetic(5) + "@" + RandomStringUtils.randomAlphabetic(5) + ".com";
        return randomEmail;
    }

    // sets a current date and time for features executed @now
    private String getDateTime() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        return dateFormat.format(date);
    }

    // additional After hook that precludes the one in the Hooks class
    // this hook uses global variables and session instantited variables to update TestRail with
    // test case verdicts and comments
    @After
    public void TearDown(Scenario scenario) throws APIException, IOException {
        TestRailService testRailService = new TestRailService(Hooks.testRailURL, Hooks.testRailLogin, Hooks.testRailPass);

        //Get the current TestPlan, based on the project and test plan name
        Project project = testRailService.getProjectByName(Hooks.testRailProject);
        TestPlan plan = project.getTestPlanByName(Hooks.testRailPlan);

        //That test plan will contain one or more test runs, each of
        // which contains multiple "Tests" (or TestInstances as we call them)

        // The TestInstance is the entity we care about when we're reporting the test result
//        List<TestRun> testRuns = plan.getTestRuns();

        //Iterate through the test runs, and build a map of "TestCase ID" to "TestInstance ID"
//        for (TestRun testRun : testRuns) {
//            System.out.println("TestRun: " + testRun.getName());
//        }

        // conditional for determining test case pass/fail status
        if (scenario.isFailed()) {
            TestResult result = new TestResult();
            result.setTestId(testId);
//            result.setVerdict("Failed");
//            result.setComment("Test Failed on " + getDateTime() + " " + testPurpose + " on environment " + apiUrl);
            result.setVerdict("Retest");
            result.setComment("Retest Required "+ getDateTime() + " " + testPurpose + " on environment " + apiUrl);
            testRailService.addTestResult(testId, result);
        } else {
            TestResult result = new TestResult();
            result.setTestId(testId);
            result.setVerdict("Passed");
            result.setComment("Test Passed on " + getDateTime() + " " + testPurpose + " on environment " + apiUrl);
//            result.setVerdict("Retest");
//            result.setComment("Retest Required "+ getDateTime() + " " + testPurpose + " on environment " + apiUrl);
            testRailService.addTestResult(testId, result);
        }

        if(Hooks.browserConfig.equals("headless")){
            System.out.println("");
        } else {
            driver.quit();
        }
    }

    @Given("^The \"(.*?)\" for the scenario is '(.+)'$")
    public void test_case_setter_ui(String conditionType, String condition) throws Throwable {
        switch (conditionType) {
            case "test case":
                this.testId = Integer.valueOf(condition);
                break;
            case "test purpose":
                if (condition.equals("*")){
                    testPurpose = Hooks.testCasePurpose;
                } else
                    testPurpose = condition;
                break;
        }
    }

    @Then("^I should see that the response types are correct for \"(.*?)\"$")
    public void then_I_validate_response(String apiNode) {
        switch (apiNode) {
            default:
                break;
        }
    }

    @And("^I wait for a long time$")
    public void and_I_wait() throws InterruptedException {
        TimeUnit.MINUTES.sleep(2);
    }
}